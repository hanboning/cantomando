import json, os, sys

path = os.path.join(os.path.dirname(__file__), 'data/hangulRomanization.json')
hangul_romanized = json.load(open(path))

# order matters, xx should precede x for greedy matching
INITIALS = ['b','ch','d','g','h','jj','j','kk','k','m','n','pp','p','r','ss','s','tt','t','']

finals = set()

for roman in hangul_romanized['mct']:
  for initial in INITIALS:
    pos = roman.find(initial)
    if pos == 0:
      final = roman[len(initial):]
      finals.add(final)
      break
initials_finals = { 'initials': sorted(INITIALS), 'finals': sorted(list(finals)) }
print json.dumps(initials_finals)
