# -*- coding: utf-8 -*-
import json, sys
import requests
from bs4 import BeautifulSoup

def character_to_hanguls(char):
  req = requests.get('http://hanja.naver.com/hanja?q={}'.format(char))
  text = BeautifulSoup(req.text).find(id='content').find('dl').find('dd').find('strong').text
  hanguls = []
  for hangul_part in text.split(','):
    hanguls.append(hangul_part.split(' ')[-1])
  return ' '.join(hanguls)

if __name__ == '__main__':
  if len(sys.argv) != 2:
    print 'Usage: python naver.py <json with top level character keys>'
    sys.exit(1)
  with open(sys.argv[1]) as fp:
    for char in json.load(fp).iterkeys():
      char = char.encode('utf-8')
      try:
        hanguls = character_to_hanguls(char).encode('utf-8')
        print char, hanguls
      except (KeyboardInterrupt, SystemExit): raise
      except:
        print char
