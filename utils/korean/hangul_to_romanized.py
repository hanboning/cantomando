# -*- coding: utf-8 -*-
import json, os, sys

if len(sys.argv) != 2:
  print 'Usage: python <this file>.py <charHanguls.json>'
path = os.path.join(os.path.dirname(__file__), 'data/hangulRomanization.json')
hangul_romanized = json.load(open(path))
hangul_mct = {}
for hangul, mct in zip(hangul_romanized['hangul'], hangul_romanized['mct']):
  hangul_mct[hangul] = mct
char_sounds = {}
for char, hanguls in json.load(open(sys.argv[1])).iteritems():
  char_sounds[char] = [hangul_mct[hangul] for hangul in hanguls]
print json.dumps(char_sounds)
