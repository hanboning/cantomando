import json, sys

if len(sys.argv) != 2:
  print 'Usage: python <this file>.py <charHanguls.json>'
unique_hanguls = set()
for hanguls in json.load(open(sys.argv[1])).itervalues():
  for hangul in hanguls: unique_hanguls.add(hangul)
print json.dumps({ "hangul": list(unique_hanguls) }, ensure_ascii=False, encoding='utf-8')
