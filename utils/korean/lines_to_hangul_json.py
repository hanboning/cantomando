import json, sys

def line_to_hangul(line):
  parts = line.split(' ')
  hanguls = []
  for hangul_unprocessed in parts[1:]:
    for hangul in hangul_unprocessed.split('('):
      hanguls.append(hangul.replace(')', ''))
  char = parts[0]
  return char, hanguls

if __name__ == '__main__':
  if len(sys.argv) != 2:
    print 'Usage: python <this file>.py <file with scraped lines>'
  char_hanguls = {}
  for line in open(sys.argv[1]):
    char, hanguls = line_to_hangul(line.strip())
    char_hanguls[char] = hanguls
  print json.dumps(char_hanguls)
