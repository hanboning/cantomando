# -*- coding: utf-8 -*-
import json, sys

INITIALS = [u'b',u'ch',u'c',u'd',u'đ',u'gh',u'gi',u'g',u'h',u'kh',u'k',u'l',u'm',u'ngh',u'ng',u'nh',u'n',u'ph',u'p',u'qu',u'r',u's',u'th',u'tr',u't',u'v',u'x']

if __name__ == '__main__':
  if len(sys.argv) != 2:
    print 'Usage: python <this file>.py <charSounds json>'
  finals = set()
  for char, sounds in json.load(open(sys.argv[1])).iteritems():
    for sound in sounds:
      for initial in INITIALS:
        pos = sound.find(initial)
        if pos == 0:
          final = sound[len(initial):-1]
          finals.add(final)
          break;
  print json.dumps(sorted(list(finals)))
