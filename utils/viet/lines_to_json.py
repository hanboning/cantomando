# -*- coding: utf-8 -*-
import ast, json, sys

DIACRITIC_MAP = {
  1: [u'a',u'ă',u'â',u'e',u'ê',u'i',u'o',u'ô',u'ơ',u'u',u'ư',u'y'],
  2: [u'à',u'ằ',u'ầ',u'è',u'ề',u'ì',u'ò',u'ồ',u'ờ',u'ù',u'ừ',u'ỳ'],
  3: [u'ả',u'ẳ',u'ẩ',u'ẻ',u'ể',u'ỉ',u'ỏ',u'ổ',u'ở',u'ủ',u'ử',u'ỷ'],
  4: [u'ã',u'ẵ',u'ẫ',u'ẽ',u'ễ',u'ĩ',u'õ',u'ỗ',u'ỡ',u'ũ',u'ữ',u'ỹ'],
  5: [u'á',u'ắ',u'ấ',u'é',u'ế',u'í',u'ó',u'ố',u'ớ',u'ú',u'ứ',u'ý'],
  6: [u'ạ',u'ặ',u'ậ',u'ẹ',u'ệ',u'ị',u'ọ',u'ộ',u'ợ',u'ụ',u'ự',u'ỵ'],
}

def extract_line(line, diacritic_map=None):
  if diacritic_map is None:
    pos = line.find(' ')
    char = line[:pos]
    return char, ast.literal_eval(line[pos+1:])
  pos = line.find(' ')
  char = line[:pos]
  sounds = []
  for sound in ast.literal_eval(line[pos+1:]):
    if len(sound) == 0: continue
    tone = 0
    for tone_number, diacritic_vowel_list in diacritic_map.iteritems():
      for diacritic_vowel, non_diacritic_vowel in zip(diacritic_vowel_list, diacritic_map[1]):
        pos = sound.find(diacritic_vowel)
        if pos >= 0:
          sound = sound.replace(diacritic_vowel, non_diacritic_vowel)
          tone = tone_number
    sounds.append('{}{}'.format(sound.encode('utf-8'), tone))
  return char, sounds

if __name__ == '__main__':
  if len(sys.argv) != 2:
    print 'Usage: python <this file>.py <line by line scraped file>'
  char_sounds = {}
  for line in open(sys.argv[1]):
    char, sounds = extract_line(line.strip(), DIACRITIC_MAP)
    #char, sounds = extract_line(line.strip())
    char_sounds[char] = sounds
  print json.dumps(char_sounds)
