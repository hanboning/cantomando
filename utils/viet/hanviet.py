# -*- coding: utf-8 -*-
import json, sys
import requests

def character_to_sounds(char):
  req = requests.get('http://www.hanviet.org/ajax.php?query={}&methode=exact'.format(char))
  req.encoding = 'utf-8'
  return req.text[req.text.find('=')+1:-1].split(', ')

if __name__ == '__main__':
  if len(sys.argv) != 2:
    print 'Usage: python <this file>.py <json with top level character keys>'
    sys.exit(1)
  with open(sys.argv[1]) as fp:
    for char in json.load(fp).iterkeys():
      char = char.encode('utf-8')
      sounds = character_to_sounds(char)
      print '{} {}'.format(char, sounds)
