import pickle
import sys
import json

def format(data):
  res = {}
  for sound, othersounds in data.iteritems():
#    ttl = 0
#    for charlist in othersounds.itervalues():
#      ttl += len(set(charlist))
    data = []
    for othersound, charlist in othersounds.iteritems():
      lst = list(set(charlist))
      data.append({
        'sound': othersound,
        'chars': lst,
#        'numchars': len(lst),
#        'freq': len(lst)/float(ttl)
      })
    res[sound] = data
  return res

if __name__ == '__main__':
  if (len(sys.argv) != 2):
    print('Usage: python pickle2json.py <pickled data file>')
    sys.exit(1)
  data = pickle.load(open(sys.argv[1]))
  res = format(data)
  print json.dumps(res)
