# -*- coding: utf-8 -*-
import json, sys
import requests
from bs4 import BeautifulSoup

def character_to_sounds(ch):
  url = 'http://twblg.dict.edu.tw/holodict_new/result.jsp?radiobutton=1&limit=100&querytarget=1&sample={}'
  req = requests.get(url.format(ch))
  sounds = []
  for tr in BeautifulSoup(req.text).find('table').find_all('tr')[1:]: # for all rows except header row
    td1, td2 = tr.find_all('td')[1:-1]
    try: character = td1.find('a').text.strip()
    except AttributeError: character = td1.text.strip()
    if character.encode('utf-8') != ch: continue
    sound = td2.text.strip()
    sounds.append(sound)
  return sounds

if __name__ == '__main__':
  if len(sys.argv) != 3:
    print 'Usage: python tw_minnan.py <json with top level character keys> <output file>'
  with open(sys.argv[1]) as in_fp:
    with open(sys.argv[2], 'w') as out_fp:
      for char in json.load(in_fp).iterkeys():
        char = char.encode('utf-8')
        try:
          sounds = character_to_sounds(char)
          out_fp.write('{} {}\n'.format(char, sounds))
          print char, sounds
        except (KeyboardInterrupt, SystemExit): raise
        except:
          out_fp.write('{} {}\n'.format(char, []))
          print char, []
