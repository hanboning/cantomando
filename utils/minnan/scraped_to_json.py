# -*- coding: utf-8 -*-
import ast, json, sys

DIACRITIC_MAP = {
  'á': ('a', 2), 'à': ('a', 3), 'â': ('a', 5), 'ā': ('a', 7), 'a̍': ('a', 8),
  'é': ('e', 2), 'è': ('e', 3), 'ê': ('e', 5), 'ē': ('e', 7), 'e̍': ('e', 8),
  'í': ('i', 2), 'ì': ('i', 3), 'î': ('i', 5), 'ī': ('i', 7), 'i̍': ('i', 8),
  'ó': ('o', 2), 'ò': ('o', 3), 'ô': ('o', 5), 'ō': ('o', 7), 'o̍': ('o', 8),
  'ú': ('u', 2), 'ù': ('u', 3), 'û': ('u', 5), 'ū': ('u', 7), 'u̍': ('u', 8),
  'ḿ': ('m', 2), 'm̂': ('m', 5), 'm̄': ('m', 7),
  'ń': ('n', 2), 'ǹ': ('n', 3), 'n̂': ('n', 5), 'n̄': ('n', 7), 'n̍': ('n', 8),
}

def extract_line(line, diacritic_map):
  pos = line.find(' ')
  char = line[:pos]
  diacritic_sounds = []
  for unprocessed_diacritic in ast.literal_eval(line[pos+1:]):
    diacritic_sounds.extend(map(lambda e: e.lower(), unprocessed_diacritic.split('/')))
  sounds = []
  for sound in set(diacritic_sounds):
    sound = sound.replace('--', '') # 乎 et al are preceeded by --
    if sound.find('-') >= 0: continue # ignore incorrectly scraped multi-character sounds
    tone = 1
    for diacritic, (non_diacritic, matching_tone) in diacritic_map.iteritems(): # replace diacritics
      diacritic = diacritic.decode('utf-8')
      if sound.find(diacritic) >= 0:
        sound = sound.replace(diacritic, non_diacritic)
        tone = matching_tone
    if sound[-1] in ['p', 't', 'k', 'h'] and tone == 1: tone = 4
    sounds.append('{}{}'.format(sound, tone))
  return char, sounds

if __name__ == '__main__':
  if len(sys.argv) != 2:
    print 'Usage: python scraped_to_json.py <line by line scraped file>'
  with open(sys.argv[1]) as fp:
    char_sounds = {}
    for line in fp:
      char, sounds = extract_line(line.strip(), DIACRITIC_MAP)
      char_sounds[char] = sounds
    print json.dumps(char_sounds)
