from operator import itemgetter
import pickle
import sys

def viewer(data):
  for sound in sorted(data):
    temp = []
    ttl = 0
    for othersound, charlist in data[sound].iteritems():
      ttl += len(charlist)
      temp.append((othersound, len(charlist)))
    temp = sorted(temp, key=itemgetter(1), reverse=True)
    for othersound, n in temp:
      if n/float(ttl) < 0.1: continue
      print '{0} => {1} {2:.0f}%'.format(sound, othersound, n/float(ttl)*100)
    #temp = map(lambda (s,n): (s, n/float(ttl), n), temp)
    #print sound
    print

if __name__ == '__main__':
  if (len(sys.argv) != 2):
    print('Usage: python soundviewer.py <pickled data file>')
    sys.exit(1)
  data = pickle.load(open(sys.argv[1]))
  viewer(data)
  print len(data)
