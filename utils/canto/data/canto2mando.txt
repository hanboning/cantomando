aa => a 50%
aa => ya 46%
aa => e 4%

aai => ai 86%
aai => yi 14%

aak => e 80%
aak => wo 20%

aam => yan 100%

aan => yan 100%

aang => ying 100%

aap => ya 100%

aat => ya 67%
aat => e 17%
aat => an 17%

aau => ao 71%
aau => niu 29%

ai => ai 50%
ai => yi 25%
ai => xi 25%

ak => e 50%
ak => wo 33%
ak => ya 17%

am => an 100%

an => en 100%

ang => geng 60%
ang => ying 40%

ap => xi 50%
ap => yan 50%

au => ou 85%
au => qu 8%
au => ao 8%

baa => ba 86%
baa => pa 10%
baa => bo 5%

baai => bai 80%
baai => bei 20%

baak => bo 47%
baak => bai 29%
baak => mo 12%
baak => pai 6%
baak => po 6%

baan => ban 79%
baan => qiang 11%
baan => pan 11%

baang => beng 100%

baat => ba 100%

baau => bao 71%
baau => pao 29%

bai => bi 78%
bai => bie 11%
bai => bo 11%

bak => bei 100%

bam => pang 50%
bam => beng 50%

ban => bin 56%
ban => ben 28%
ban => bing 11%
ban => pin 6%

bang => beng 80%
bang => ping 20%

bat => bi 40%
bat => bu 20%
bat => ba 20%
bat => bo 10%
bat => fu 10%

be => pi 100%

bei => bi 63%
bei => bei 14%
bei => pi 11%
bei => mi 9%
bei => fei 3%

bek => bi 100%

beng => bing 83%
beng => ping 17%

beu => bao 100%

bi => pi 50%
bi => bi 50%

bik => bi 71%
bik => pai 14%
bik => po 14%

bin => bian 87%
bin => pian 13%

bing => bing 79%
bing => ping 11%
bing => ning 5%
bing => beng 5%

bit => bie 78%
bit => bi 11%
bit => pie 11%

biu => biao 86%
biu => bao 7%
biu => shao 7%

bo => bo 69%
bo => ban 8%
bo => fan 8%
bo => po 8%
bo => pan 8%

bok => bo 61%
bok => bao 17%
bok => fu 6%
bok => jue 6%
bok => xue 6%
bok => po 6%

bong => bang 69%
bong => pang 31%

bou => bu 42%
bou => bao 39%
bou => pu 15%
bou => nao 3%

bui => bei 100%

buk => pu 58%
buk => bao 25%
buk => fu 8%
buk => bu 8%

bun => pan 40%
bun => ban 33%
bun => ben 13%
bun => pang 7%
bun => bo 7%

bung => beng 100%

but => bo 90%
but => bi 10%

caa => cha 81%
caa => zha 10%
caa => ci 5%
caa => chai 5%

caai => chai 38%
caai => cha 15%
caai => cai 15%
caai => ci 8%
caai => chuai 8%
caai => chuan 8%
caai => cuo 8%

caak => ce 36%
caak => zha 27%
caak => zei 18%
caak => shan 9%
caak => chai 9%

caam => chan 39%
caam => can 33%
caam => zan 6%
caam => shen 6%
caam => cen 6%
caam => sha 6%
caam => shan 6%

caan => chan 50%
caan => can 50%

caang => cheng 85%
caang => chang 15%

caap => cha 100%

caat => cha 33%
caat => ca 22%
caat => shua 22%
caat => sha 11%
caat => ta 11%

caau => chao 64%
caau => jiao 18%
caau => cao 9%
caau => zhou 9%

cai => qi 71%
cai => qie 18%
cai => xi 6%
cai => ji 6%

cak => ce 100%

cam => xun 36%
cam => qin 18%
cam => chen 18%
cam => zan 9%
cam => shen 9%
cam => can 9%

can => chen 40%
can => qing 13%
can => zhen 13%
can => qin 13%
can => cheng 13%
can => shen 7%

cang => ceng 60%
cang => zeng 20%
cang => cheng 20%

cap => qi 50%
cap => ji 50%

cat => qi 100%

cau => chou 62%
cau => qiu 21%
cau => xiu 12%
cau => cou 4%

ce => che 21%
ce => chi 21%
ce => xie 16%
ce => she 11%
ce => ju 11%
ce => yi 5%
ce => duo 5%
ce => qie 5%
ce => ye 5%

cek => chi 75%
cek => che 25%

ceng => qing 100%

ceoi => chui 28%
ceoi => chu 22%
ceoi => qu 16%
ceoi => cui 16%
ceoi => chuai 6%
ceoi => chuan 3%
ceoi => sui 3%
ceoi => zhui 3%
ceoi => xu 3%

ceon => chun 56%
ceon => xun 33%
ceon => qin 11%

ceot => chu 100%

ci => chi 36%
ci => ci 27%
ci => shi 11%
ci => cha 3%
ci => zi 3%
ci => duo 3%
ci => ce 3%
ci => zhi 3%
ci => yi 2%
ci => chai 2%
ci => si 2%
ci => zhe 2%
ci => tuo 2%
ci => qi 2%
ci => di 2%

cik => chi 62%
cik => qi 12%
cik => she 12%
cik => cu 12%

cim => qian 40%
cim => xian 30%
cim => jian 20%
cim => chan 10%

cin => qian 53%
cin => jian 24%
cin => chan 12%
cin => zan 6%
cin => can 6%

cing => cheng 48%
cing => qing 30%
cing => chen 9%
cing => jing 4%
cing => zheng 4%
cing => deng 4%

cip => qie 100%

cit => qie 33%
cit => che 33%
cit => qi 11%
cit => zhe 11%
cit => she 11%

ciu => qiao 56%
ciu => chao 19%
ciu => zhao 12%
ciu => shao 6%
ciu => xiao 6%

co => chu 46%
co => cuo 38%
co => cu 8%
co => zuo 8%

coek => chuo 30%
coek => zhuo 30%
coek => jue 10%
coek => chao 10%
coek => xue 10%
coek => shao 10%

coeng => chang 51%
coeng => qiang 29%
coeng => xiang 9%
coeng => zhang 3%
coeng => chuang 3%
coeng => cheng 3%
coeng => pan 3%

coi => cai 76%
coi => sai 14%
coi => se 5%
coi => zai 5%

cok => cuo 33%
cok => cu 33%
cok => zhuo 33%

cong => chuang 48%
cong => cang 26%
cong => chang 13%
cong => tong 4%
cong => zang 4%
cong => zhuang 4%

cou => cao 38%
cou => zao 29%
cou => cu 12%
cou => cuo 8%
cou => qu 4%
cou => qiu 4%
cou => chu 4%

cuk => chu 31%
cuk => cu 19%
cuk => chuo 12%
cuk => xu 12%
cuk => shu 6%
cuk => su 6%
cuk => qi 6%
cuk => chong 6%

cung => chong 38%
cung => cong 34%
cung => zhong 14%
cung => song 7%
cung => gong 3%
cung => zong 3%

cyu => chu 64%
cyu => zhu 21%
cyu => shu 14%

cyun => chuan 26%
cyun => cun 18%
cyun => quan 15%
cyun => chuai 9%
cyun => cuan 9%
cyun => chun 6%
cyun => zan 3%
cyun => dun 3%
cyun => guan 3%
cyun => zhuan 3%
cyun => shun 3%
cyun => yuan 3%

cyut => duo 33%
cyut => cuo 17%
cyut => zu 17%
cyut => zuo 17%
cyut => cu 17%

daa => da 100%

daai => dai 67%
daai => da 11%
daai => e 11%
daai => can 11%

daam => dan 100%

daan => dan 75%
daan => tan 12%
daan => shan 6%
daan => chan 6%

daap => da 56%
daap => ta 38%
daap => die 6%

daat => da 83%
daat => ta 17%

dai => di 71%
dai => dai 10%
dai => ti 10%
dai => chi 5%
dai => li 5%

dak => de 60%
dak => dei 20%
dak => te 20%

dam => shen 25%
dam => dan 25%
dam => jing 25%
dam => beng 25%

dan => dun 71%
dan => den 29%

dang => deng 94%
dang => cheng 6%

dap => da 100%

dat => tu 100%

dau => dou 90%
dau => jiu 5%
dau => du 5%

de => dia 75%
de => die 25%

dei => di 50%
dei => dei 20%
dei => de 20%
dei => deng 10%

dek => di 75%
dek => ti 25%

deng => ding 88%
deng => di 12%

deoi => dui 80%
deoi => zhui 10%
deoi => dun 10%

deon => dun 80%
deon => cun 10%
deon => dui 10%

deot => duo 100%

deu => diao 100%

di => jie 33%
di => ji 33%
di => di 33%

dik => di 92%
dik => de 8%

dim => dian 100%

din => dian 89%
din => tian 11%

ding => ding 84%
ding => ting 16%

dip => die 100%

dit => die 33%
dit => zhi 17%
dit => jie 17%
dit => ji 17%
dit => di 17%

diu => diao 87%
diu => diu 7%
diu => tiao 7%

do => duo 86%
do => chi 14%

doe => duo 100%

doek => zhuo 67%
doek => duo 33%

doeng => zhuo 100%

doi => dai 100%

dok => duo 67%
dok => du 33%

dong => dang 100%

dou => dao 55%
dou => du 33%
dou => duo 6%
dou => tao 3%
dou => dou 3%

duk => du 80%
duk => dai 10%
duk => dou 10%

dung => dong 75%
dung => tong 19%
dung => zhong 6%

dut => du 100%

dyun => duan 100%

dyut => du 50%
dyut => duo 50%

faa => hua 83%
faa => kua 17%

faai => kuai 60%
faai => gui 20%
faai => kui 20%

faan => fan 92%
faan => pan 6%
faan => ban 3%

faat => fa 100%

fai => fei 56%
fai => hui 25%
fai => xun 6%
fai => hun 6%
fai => bi 6%

fan => fen 62%
fan => xun 22%
fan => hun 9%
fan => kun 3%
fan => pen 3%

fang => hong 50%
fang => ben 50%

fat => fa 33%
fat => fu 27%
fat => hu 20%
fat => ku 7%
fat => fei 7%
fat => fo 7%

fau => fu 38%
fau => fou 25%
fau => bu 12%
fau => pou 12%
fau => pi 12%

fe => fei 100%

fei => fei 96%
fei => pei 4%

fing => hong 50%
fing => ben 50%

fit => fu 100%

fo => huo 42%
fo => ke 42%
fo => xi 8%
fo => fang 8%

fok => jue 50%
fok => huo 25%
fok => fu 25%

fong => fang 57%
fong => huang 30%
fong => kuang 7%
fong => xi 3%
fong => pang 3%

fu => fu 78%
fu => ku 9%
fu => hu 9%
fu => pu 4%

fui => hui 62%
fui => kui 25%
fui => wei 12%

fuk => fu 100%

fun => kuan 67%
fun => huan 17%
fun => guan 17%

fung => feng 86%
fung => peng 7%
fung => ping 3%
fung => beng 3%

fut => kuo 100%

gaa => jia 67%
gaa => ga 18%
gaa => qie 6%
gaa => xia 3%
gaa => gu 3%
gaa => ka 3%

gaai => jie 68%
gaai => xie 12%
gaai => jia 4%
gaai => kai 4%
gaai => ji 4%
gaai => gai 4%
gaai => ga 4%

gaak => ge 83%
gaak => ji 8%
gaak => ga 8%

gaam => jian 62%
gaam => gan 31%
gaam => qian 8%

gaan => jian 100%

gaang => geng 100%

gaap => jia 80%
gaap => ge 10%
gaap => xie 10%

gaat => ya 50%
gaat => zha 25%
gaat => ga 25%

gaau => jiao 80%
gaau => xiao 7%
gaau => qiao 3%
gaau => hao 3%
gaau => gao 3%
gaau => jue 3%

gai => ji 79%
gai => jie 21%

gam => jin 33%
gam => gan 26%
gam => nen 7%
gam => nin 7%
gam => ren 7%
gam => qin 4%
gam => han 4%
gam => qian 4%
gam => dan 4%
gam => xian 4%

gan => jin 81%
gan => gen 19%

gang => geng 100%

gap => ge 38%
gap => jia 25%
gap => ha 12%
gap => ji 12%
gap => he 12%

gat => jie 38%
gat => ge 25%
gat => ji 25%
gat => ju 12%

gau => jiu 52%
gau => gou 39%
gau => ju 3%
gau => huo 3%
gau => hou 3%

ge => ge 50%
ge => kai 50%

gei => ji 89%
gei => qi 11%

geng => jing 60%
geng => geng 20%
geng => qing 20%

geoi => ju 77%
geoi => gou 8%
geoi => qu 4%
geoi => che 4%
geoi => gui 4%
geoi => ji 4%

gep => xie 50%
gep => jia 50%

gi => ji 100%

gik => ji 88%
gik => ge 12%

gim => jian 100%

gin => jian 90%
gin => xian 10%

ging => jing 81%
ging => jin 11%
ging => qiang 4%
ging => yuan 4%

gip => xia 20%
gip => jie 20%
gip => xie 20%
gip => jia 20%
gip => qie 10%
gip => se 10%

git => jie 88%
git => ji 12%

giu => jiao 80%
giu => qiao 10%
giu => zhuo 10%

go => ge 86%
go => he 14%

goe => ju 100%

goek => jiao 100%

goeng => jiang 62%
goeng => qiang 38%

goi => gai 100%

gok => ge 50%
gok => jiao 25%
gok => jue 25%

gon => gan 91%
gon => qian 9%

gong => gang 68%
gong => jiang 16%
gong => kang 8%
gong => xiang 4%
gong => hong 4%

got => ge 100%

gou => gao 100%

gu => gu 89%
gu => tu 6%
gu => gua 3%
gu => jia 3%

gui => gui 50%
gui => guai 50%

guk => ju 50%
guk => gu 40%
guk => gao 10%

gun => guan 92%
gun => wan 8%

gung => gong 100%

gwaa => gua 88%
gwaa => gu 12%

gwaai => guai 100%

gwaak => guo 50%
gwaak => guai 50%

gwaan => guan 88%
gwaan => chuan 12%

gwaang => guang 100%

gwaat => gua 67%
gwaat => kuo 33%

gwai => gui 78%
gwai => kui 11%
gwai => ji 7%
gwai => ju 4%

gwan => jun 46%
gwan => kun 23%
gwan => gun 23%
gwan => yun 8%

gwang => hong 50%
gwang => gong 50%

gwat => jue 45%
gwat => gu 27%
gwat => ju 9%
gwat => hua 9%
gwat => hu 9%

gwik => xi 50%
gwik => qu 50%

gwing => jiong 100%

gwo => guo 60%
gwo => wo 10%
gwo => guan 10%
gwo => gua 10%
gwo => ge 10%

gwok => guo 62%
gwok => kuo 12%
gwok => hun 12%
gwok => luo 12%

gwong => guang 100%

gyun => juan 73%
gyun => quan 20%
gyun => xuan 7%

gyut => jue 100%

haa => xia 54%
haa => ha 21%
haa => he 14%
haa => jia 4%
haa => sha 4%
haa => ge 4%

haai => xie 53%
haai => hai 24%
haai => jie 12%
haai => kai 6%
haai => ke 6%

haak => ke 40%
haak => he 20%
haak => xia 10%
haak => ka 10%
haak => kei 10%
haak => hei 10%

haam => xian 50%
haam => han 30%
haam => kan 10%
haam => jian 10%

haan => xian 83%
haan => qian 17%

haang => hang 50%
haang => xing 25%
haang => keng 25%

haap => xia 89%
haap => jia 11%

haau => xiao 37%
haau => kao 26%
haau => qiao 16%
haau => jiao 16%
haau => hou 5%

hai => xi 82%
hai => qi 9%
hai => ji 9%

hak => ke 67%
hak => hei 11%
hak => kei 11%
hak => he 11%

ham => kan 59%
ham => han 21%
ham => xian 7%
ham => qu 7%
ham => qian 7%

han => hen 67%
han => ken 33%

hang => xing 33%
hang => heng 25%
hang => hang 25%
hang => ken 8%
hang => jing 8%

hap => ke 36%
hap => he 29%
hap => xia 14%
hap => qia 14%
hap => ge 7%

hat => qi 29%
hat => xia 29%
hat => he 29%
hat => hu 14%

hau => hou 88%
hau => hong 6%
hau => kou 6%

hei => xi 53%
hei => qi 34%
hei => yi 3%
hei => hai 3%
hei => gai 3%
hei => hei 3%

hek => chi 100%

heng => qing 100%

heoi => xu 47%
heoi => qu 27%
heoi => wei 7%
heoi => hu 7%
heoi => shi 7%
heoi => yu 7%

him => qian 80%
him => xian 20%

hin => xian 42%
hin => qian 33%
hin => xuan 8%
hin => yan 8%
hin => jian 8%

hing => qing 45%
hing => xing 36%
hing => xiong 9%
hing => xin 9%

hip => xie 43%
hip => qie 29%
hip => qian 14%
hip => jia 14%

hit => xie 80%
hit => qie 20%

hiu => xiao 44%
hiu => qiao 33%
hiu => jiao 22%

ho => he 62%
ho => ke 33%
ho => ge 5%

hoe => shi 33%
hoe => xu 33%
hoe => xue 33%

hoeng => xiang 92%
hoeng => shang 8%

hoi => hai 56%
hoi => kai 33%
hoi => hei 11%

hok => xue 25%
hok => qiao 25%
hok => ke 25%
hok => he 25%

hom => kan 67%
hom => qu 33%

hon => han 71%
hon => kan 25%
hon => an 4%

hong => hang 32%
hong => kang 21%
hong => kuang 16%
hong => xiang 16%
hong => xing 5%
hong => qiang 5%
hong => jiang 5%

hot => he 80%
hot => ke 20%

hou => hao 95%
hou => ke 5%

huk => ku 100%

hung => hong 39%
hung => xiong 25%
hung => kong 21%
hung => gong 7%
hung => hou 4%
hung => xiu 4%

hyun => xuan 38%
hyun => quan 31%
hyun => juan 12%
hyun => qiong 6%
hyun => huan 6%
hyun => yuan 6%

hyut => xue 50%
hyut => xie 50%

jaa => nian 40%
jaa => ye 40%
jaa => shi 20%

jaai => chuan 33%
jaai => cai 33%
jaai => chuai 33%

jaak => chi 100%

jaam => zhan 100%

jaau => ao 100%

jai => yi 38%
jai => ye 38%
jai => zhuai 25%

jam => yin 40%
jam => ren 36%
jam => yan 8%
jam => nen 4%
jam => qin 4%
jam => nin 4%
jam => lin 4%

jan => yin 50%
jan => ren 22%
jan => xin 12%
jan => yan 6%
jan => en 3%
jan => zhen 3%
jan => yun 3%

jap => yi 50%
jap => ru 25%
jap => qi 25%

jat => yi 88%
jat => ri 12%

jau => you 71%
jau => qiu 12%
jau => xiu 6%
jau => rou 6%
jau => yao 2%
jau => xu 2%

je => ye 55%
je => ruo 10%
je => re 10%
je => nian 10%
je => yi 5%
je => shi 5%
je => xie 5%

jeng => ying 100%

jeoi => rui 60%
jeoi => yi 20%
jeoi => zhui 20%

jeon => run 100%

ji => yi 69%
ji => er 16%
ji => ni 5%
ji => reng 3%
ji => neng 2%
ji => she 2%
ji => ning 2%
ji => qi 2%

jik => yi 81%
jik => ye 7%
jik => ai 4%
jik => xi 4%
jik => ni 4%

jim => yan 81%
jim => ran 15%
jim => xian 4%

jin => yan 67%
jin => xian 12%
jin => yin 6%
jin => jian 4%
jin => ran 4%
jin => ye 4%
jin => yuan 2%

jing => ying 74%
jing => xing 13%
jing => reng 3%
jing => jia 3%
jing => ren 3%
jing => ning 3%

jip => ye 44%
jip => yan 22%
jip => xie 11%
jip => nie 11%
jip => shou 11%

jit => ye 38%
jit => yan 25%
jit => nie 25%
jit => re 12%

jiu => yao 70%
jiu => rao 24%
jiu => tao 6%

joek => yue 40%
joek => yao 20%
joek => ruo 13%
joek => re 7%
joek => xue 7%
joek => nve 7%
joek => yu 7%

joeng => yang 62%
joeng => rang 28%
joeng => niang 3%
joeng => chang 3%
joeng => ying 3%

juk => yu 58%
juk => ru 12%
juk => rou 8%
juk => hao 4%
juk => xuan 4%
juk => ao 4%
juk => wo 4%
juk => xu 4%

jung => yong 54%
jung => rong 38%
jung => weng 8%

jyu => yu 79%
jyu => ru 11%
jyu => kan 3%
jyu => xu 3%
jyu => qu 1%
jyu => ya 1%
jyu => wei 1%
jyu => hu 1%

jyun => yuan 56%
jyun => xuan 14%
jyun => wan 12%
jyun => yan 6%
jyun => xian 4%
jyun => ruan 4%
jyun => qian 2%
jyun => huan 2%

jyut => yue 53%
jyut => yi 13%
jyut => xue 13%
jyut => yin 7%
jyut => shuo 7%
jyut => shui 7%

kaa => ka 67%
kaa => qia 17%
kaa => ga 17%

kaai => kai 50%
kaai => jie 50%

kaan => ke 100%

kaat => ji 100%

kaau => kao 100%

kai => qi 50%
kai => xi 20%
kai => ji 20%
kai => qie 10%

kam => jin 45%
kam => qin 45%
kam => kan 9%

kan => qin 67%
kan => jin 33%

kang => geng 100%

kap => ji 60%
kap => xi 20%
kap => gei 10%
kap => she 10%

kat => hai 25%
kat => ka 25%
kat => ge 25%
kat => ke 25%

kau => qiu 24%
kau => kou 24%
kau => jiu 16%
kau => gou 16%
kau => rou 8%
kau => ju 4%
kau => kao 4%
kau => chou 4%

ke => qie 33%
ke => jia 33%
ke => qi 11%
ke => que 11%
ke => ji 11%

kei => qi 73%
kei => ji 24%
kei => zhi 3%

kek => ju 50%
kek => ji 50%

keoi => qu 56%
keoi => ju 31%
keoi => gou 6%
keoi => ou 6%

kik => ji 100%

kim => qian 100%

kin => qian 67%
kin => gan 33%

king => qing 33%
king => qiong 22%
king => jing 11%
king => ning 11%
king => huan 11%
king => yuan 11%

kit => jie 42%
kit => xie 17%
kit => qie 17%
kit => xia 8%
kit => ji 8%
kit => qi 8%

kiu => qiao 93%
kiu => jiao 7%

koek => xue 33%
koek => que 33%
koek => jue 33%

koeng => qiang 57%
koeng => jiang 43%

koi => gai 86%
koi => kai 14%

kok => que 40%
kok => he 40%
kok => ke 20%

kong => kang 46%
kong => kuang 31%
kong => gang 15%
kong => kuo 8%

ku => gu 100%

kui => hui 38%
kui => kuai 31%
kui => gui 15%
kui => kwai 8%
kui => kui 8%

kuk => qu 100%

kung => qiong 100%

kut => kuo 25%
kut => gua 25%
kut => huo 25%
kut => hua 25%

kwaa => kua 83%
kwaa => gua 17%

kwaai => xie 100%

kwaak => ke 100%

kwaang => kuang 67%
kwaang => guang 33%

kwai => kui 73%
kwai => qiu 9%
kwai => gui 9%
kwai => xie 9%

kwan => kun 47%
kwan => jun 16%
kwan => qun 16%
kwan => gun 11%
kwan => shu 5%
kwan => jiong 5%

kwik => xi 100%

kwok => kuo 100%

kwong => kuang 80%
kwong => kuo 20%

kyun => quan 80%
kyun => juan 20%

kyut => jue 77%
kyut => que 23%

laa => la 91%
laa => xia 9%

laai => la 38%
laai => lai 31%
laai => luo 15%
laai => nai 8%
laai => lao 8%

laak => lei 75%
laak => le 25%

laam => lan 78%
laam => jian 11%
laam => kan 6%
laam => la 6%

laan => lan 100%

laang => lang 60%
laang => leng 40%

laap => la 73%
laap => lie 18%
laap => li 9%

laat => la 71%
laat => lie 29%

laau => lao 100%

lai => li 90%
lai => lie 10%

lak => le 67%
lak => lei 33%

lam => lin 89%
lam => lun 11%

lap => li 100%

lat => shuai 100%

lau => liu 48%
lau => lou 40%
lau => lv 10%
lau => liao 2%

le => li 100%

lei => li 95%
lei => lv 2%
lei => mao 2%

lek => le 100%

lem => shi 100%

leng => ling 44%
leng => jing 22%
leng => liang 22%
leng => lian 11%

leoi => lei 57%
leoi => lv 24%
leoi => li 13%
leoi => lu 2%
leoi => lou 2%
leoi => luo 2%

leon => lin 44%
leon => lun 44%
leon => luan 12%

leot => li 38%
leot => lv 25%
leot => yu 25%
leot => shuai 12%

li => li 100%

lik => li 83%
lik => nuo 8%
lik => le 8%

lim => lian 100%

lin => lian 88%
lin => nian 12%

ling => ling 83%
ling => leng 12%
ling => lin 4%

lip => lie 75%
lip => li 25%

lit => lie 86%
lit => xie 14%

liu => liao 84%
liu => liu 11%
liu => le 3%
liu => lao 3%

lo => luo 100%

loek => lve 100%

loeng => liang 88%
loeng => lang 8%
loeng => lia 4%

loi => lai 89%
loi => lei 11%

lok => luo 38%
lok => lao 14%
lok => ge 14%
lok => ka 5%
lok => le 5%
lok => la 5%
lok => yao 5%
lok => lu 5%
lok => ga 5%
lok => yue 5%

long => lang 84%
long => liang 11%
long => dang 5%

lou => lu 52%
lou => lao 31%
lou => liao 10%
lou => mu 2%
lou => lou 2%
lou => nu 2%

luk => lu 59%
luk => liu 24%
luk => lv 12%
luk => liao 6%

lung => long 90%
lung => nong 5%
lung => shuang 5%

lyun => luan 58%
lyun => lian 42%

lyut => lie 50%
lyut => lv 25%
lyut => luo 25%

m => wu 100%

maa => ma 96%
maa => mu 4%

maai => mai 71%
maai => yu 14%
maai => man 14%

maak => mai 50%
maak => mo 33%
maak => bo 17%

maan => man 71%
maan => wan 25%
maan => mian 4%

maang => meng 57%
maang => beng 29%
maang => mang 14%

maat => mo 67%
maat => ma 33%

maau => mao 90%
maau => mu 10%

mai => mi 89%
mai => mai 11%

mak => mo 55%
mak => mai 27%
mak => me 9%
mak => pa 9%

man => wen 75%
man => min 19%
man => meng 3%
man => mang 3%

mang => meng 83%
mang => mang 17%

mat => mi 43%
mat => wu 29%
mat => mie 14%
mat => wa 14%

mau => mou 47%
mau => miu 20%
mau => miao 13%
mau => mao 13%
mau => pi 7%

me => mie 75%
me => wai 25%

mei => mei 35%
mei => mi 35%
mei => wei 23%
mei => yi 3%
mei => man 3%

meng => ming 100%

mi => mi 100%

mik => mi 100%

min => mian 93%
min => wan 7%

ming => ming 94%
ming => min 6%

mit => mie 100%

miu => miao 81%
miu => miu 6%
miu => yao 6%
miu => mou 6%

mo => mo 91%
mo => me 9%

mok => mo 62%
mok => mu 12%
mok => bao 12%
mok => bo 12%

mong => wang 53%
mong => mang 41%
mong => meng 6%

mou => wu 41%
mou => mu 24%
mou => mao 19%
mou => mo 11%
mou => lao 3%
mou => mou 3%

mui => mei 100%

muk => mu 100%

mun => men 75%
mun => man 25%

mung => meng 100%

mut => mo 71%
mut => ma 14%
mut => mie 7%
mut => mei 7%

naa => na 70%
naa => ne 20%
naa => nuo 10%

naai => nai 50%
naai => ni 33%
naai => er 17%

naam => nan 86%
naam => ran 14%

naan => nan 100%

naap => na 100%

naat => na 50%
naat => ruo 50%

naau => nao 60%
naau => rao 20%
naau => mao 20%

nai => ni 100%

nam => shen 33%
nam => nian 33%
nam => ren 33%

nan => nian 100%

nang => nai 67%
nang => neng 33%

nap => qi 33%
nap => ao 33%
nap => li 33%

nau => niu 45%
nau => rou 18%
nau => ni 9%
nau => niao 9%
nau => xiu 9%
nau => qiu 9%

ne => ni 50%
ne => ne 50%

nei => ni 56%
nei => mi 17%
nei => ne 11%
nei => er 11%
nei => nin 6%

neoi => nv 67%
neoi => nei 33%

ng => wu 100%

ngaa => ya 57%
ngaa => wa 14%
ngaa => e 7%
ngaa => di 7%
ngaa => wu 7%
ngaa => yu 7%

ngaai => ai 33%
ngaai => ya 33%
ngaai => yi 33%

ngaak => e 80%
ngaak => ni 20%

ngaam => yan 80%
ngaam => ai 20%

ngaan => yan 100%

ngaang => ying 100%

ngaat => nie 100%

ngaau => yao 40%
ngaau => xiao 20%
ngaau => ao 20%
ngaau => le 10%
ngaau => yue 10%

ngai => yi 42%
ngai => wei 42%
ngai => ni 17%

ngak => e 100%

ngam => yin 100%

ngan => yin 50%
ngan => ren 25%
ngan => en 25%

ngang => geng 100%

ngap => xi 33%
ngap => yan 33%
ngap => ji 33%

ngat => wu 80%
ngat => qi 20%

ngau => gou 45%
ngau => ou 18%
ngau => hong 9%
ngau => ju 9%
ngau => hou 9%
ngau => niu 9%

ngo => e 71%
ngo => wo 29%

ngoi => ai 33%
ngoi => dai 33%
ngoi => wai 33%

ngok => e 44%
ngok => yue 33%
ngok => yao 11%
ngok => le 11%

ngon => an 100%

ngong => ang 50%
ngong => yang 25%
ngong => zhuang 25%

ngou => ao 100%

ni => ni 50%
ni => ne 50%

nik => ni 50%
nik => niao 25%
nik => chuang 12%
nik => nuo 12%

nim => nian 83%
nim => shi 8%
nim => ren 8%

nin => nian 83%
nin => nin 17%

ning => ning 92%
ning => lin 8%

nip => nie 83%
nip => ao 17%

niu => niao 86%
niu => ni 14%

no => nuo 75%
no => na 25%

noeng => niang 100%

noi => nai 75%
noi => nei 25%

nok => nuo 100%

nong => nang 67%
nong => rang 33%

nou => nu 50%
nou => nao 50%

nung => nong 100%

nyun => nen 50%
nyun => nuan 50%

o => e 50%
o => ke 25%
o => a 12%
o => o 12%

oi => ai 100%

ok => wu 50%
ok => e 50%

on => an 71%
on => en 14%
on => e 14%

ong => ang 100%

ou => ao 60%
ou => wu 20%
ou => yu 20%

paa => pa 67%
paa => ba 28%
paa => shou 6%

paai => pai 75%
paai => bei 12%
paai => bai 12%

paak => bo 27%
paak => po 27%
paak => pa 13%
paak => pai 13%
paak => mo 7%
paak => tuo 7%
paak => bai 7%

paan => pan 60%
paan => ban 40%

paang => peng 89%
paang => bang 11%

paau => pao 71%
paau => bao 29%

pai => pi 100%

pan => pen 40%
pan => pin 30%
pan => bin 20%
pan => bi 10%

pang => ping 60%
pang => peng 20%
pang => feng 20%

pat => pi 100%

pau => pou 67%
pau => bu 33%

pe => pi 100%

pei => pi 76%
pei => bi 12%
pei => fou 4%
pei => pei 4%
pei => bei 4%

pek => pi 100%

peng => ping 100%

pet => pi 100%

pik => pi 89%
pik => bi 11%

pin => pian 65%
pin => bian 35%

ping => ping 53%
ping => pin 20%
ping => peng 13%
ping => bing 7%
ping => cheng 7%

pit => pie 100%

piu => piao 86%
piu => po 9%
piu => pu 5%

po => po 80%
po => ke 20%

pok => pu 54%
pok => po 23%
pok => pao 15%
pok => piao 8%

pong => pang 60%
pong => bang 33%
pong => fang 7%

pou => pu 60%
pou => pao 20%
pou => fu 16%
pou => bao 4%

pui => pei 60%
pui => bei 27%
pui => pai 7%
pui => fei 7%

puk => pu 100%

pun => pan 58%
pun => ban 16%
pun => bo 5%
pun => bian 5%
pun => pen 5%
pun => pang 5%
pun => fan 5%

pung => peng 86%
pung => feng 14%

put => bo 50%
put => po 50%

saa => sha 50%
saa => sa 25%
saa => suo 8%
saa => xi 8%
saa => shua 8%

saai => xi 38%
saai => sai 25%
saai => shai 25%
saai => shi 12%

saak => suo 100%

saam => shan 50%
saam => san 50%

saan => shan 41%
saan => san 23%
saan => shuan 14%
saan => chan 9%
saan => zha 5%
saan => cuan 5%
saan => can 5%

saang => sheng 83%
saang => xing 17%

saap => yi 17%
saap => zha 17%
saap => sha 17%
saap => ji 17%
saap => sa 17%
saap => se 17%

saat => sha 40%
saat => sa 30%
saat => shua 20%
saat => cha 10%

saau => shao 88%
saau => qiao 12%

sai => xi 35%
sai => shi 35%
sai => qian 4%
sai => si 4%
sai => qi 4%
sai => sa 4%
sai => shai 4%
sai => yue 4%
sai => xu 4%

sak => sai 67%
sak => se 33%

sam => shen 50%
sam => chen 14%
sam => xin 11%
sam => cen 7%
sam => qin 4%
sam => san 4%
sam => can 4%
sam => sen 4%
sam => shi 4%

san => shen 67%
san => xin 17%
san => chen 12%
san => gui 4%

sang => sheng 71%
sang => xing 29%

sap => shi 57%
sap => shen 14%
sap => she 14%
sap => se 14%

sat => shi 62%
sat => yi 12%
sat => xi 12%
sat => se 12%

sau => shou 46%
sau => xiu 22%
sau => sou 14%
sau => chou 5%
sau => sao 3%
sau => su 3%
sau => cao 3%
sau => qiu 3%
sau => shu 3%

se => she 60%
se => xie 33%
se => yi 7%

sei => si 75%
sei => xi 25%

sek => xi 67%
sek => shi 17%
sek => shuo 17%

seng => xing 50%
seng => cheng 33%
seng => sheng 17%

seoi => sui 32%
seoi => shui 18%
seoi => xu 12%
seoi => cui 10%
seoi => shu 5%
seoi => shuai 5%
seoi => rui 2%
seoi => hui 2%
seoi => shei 2%
seoi => xue 2%
seoi => shuo 2%
seoi => chui 2%
seoi => yue 2%

seon => xun 50%
seon => chun 20%
seon => shun 10%
seon => sun 10%
seon => xin 10%

seot => shu 30%
seot => shuai 30%
seot => xu 20%
seot => lv 10%
seot => qu 10%

seu => yi 50%
seu => she 50%

si => shi 48%
si => si 31%
si => zhi 5%
si => sai 3%
si => chi 3%
si => dian 2%
si => zi 2%
si => ci 2%
si => mou 2%
si => qi 2%
si => pang 2%
si => (shi 2%

sik => xi 45%
sik => shi 31%
sik => se 7%
sik => chi 3%
sik => tou 3%
sik => si 3%
sik => zhi 3%
sik => te 3%

sim => shan 50%
sim => chan 40%
sim => yan 10%

sin => xian 40%
sin => shan 32%
sin => chan 10%
sin => qian 8%
sin => dan 5%
sin => xi 2%
sin => xuan 2%

sing => sheng 49%
sing => cheng 26%
sing => xing 23%
sing => jing 3%

sip => she 62%
sip => shi 12%
sip => xi 12%
sip => zhe 12%

sit => xie 35%
sit => she 18%
sit => qie 12%
sit => zhe 12%
sit => yi 6%
sit => xue 6%
sit => qi 6%
sit => shi 6%

siu => xiao 46%
siu => shao 38%
siu => zhao 12%
siu => tiao 4%

so => suo 53%
so => shu 27%
so => sha 20%

soek => shuo 25%
soek => shao 25%
soek => xiao 12%
soek => biao 12%
soek => xue 12%
soek => zhuo 12%

soeng => xiang 36%
soeng => shang 30%
soeng => chang 15%
soeng => shuang 12%
soeng => tang 3%
soeng => long 3%

soi => sai 60%
soi => si 40%

sok => suo 38%
sok => shuo 25%
sok => shu 25%
sok => su 12%

song => sang 89%
song => shuang 11%

sou => sao 44%
sou => su 30%
sou => shu 15%
sou => shuo 7%
sou => xu 4%

suk => shu 50%
suk => su 20%
suk => xiu 10%
suk => zhu 5%
suk => suo 5%
suk => sou 5%
suk => shou 5%

sung => song 88%
sung => chong 12%

syu => shu 84%
syu => chu 16%

syun => xuan 47%
syun => suan 18%
syun => sun 18%
syun => chuan 6%
syun => zhuan 6%
syun => shun 6%

syut => xue 50%
syut => shuo 17%
syut => yue 17%
syut => shui 17%

taa => ta 100%

taai => tai 67%
taai => dai 22%
taai => di 11%

taam => tan 82%
taam => xun 9%
taam => dan 9%

taan => tan 89%
taan => dan 11%

taap => ta 80%
taap => tuo 10%
taap => da 10%

taat => ta 75%
taat => da 25%

tai => ti 71%
tai => di 24%
tai => xin 5%

tam => dang 100%

tan => tun 50%
tan => tu 10%
tan => tui 10%
tan => niao 10%
tan => qiu 10%
tan => xun 10%

tang => teng 100%

tau => tou 100%

tek => ti 100%

teng => ting 100%

teoi => tui 86%
teoi => tun 14%

teon => dun 100%

tik => ti 75%
tik => te 25%

tim => tian 100%

tin => tian 56%
tin => dian 33%
tin => tin 11%

ting => ting 80%
ting => ding 10%
ting => deng 10%

tip => tie 100%

tit => tie 50%
tit => die 50%

tiu => tiao 90%
tiu => diao 10%

to => tuo 79%
to => duo 14%
to => dai 7%

toe => tuo 100%

toi => tai 88%
toi => dai 12%

tok => tuo 67%
tok => po 17%
tok => ta 17%

tong => tang 77%
tong => shang 5%
tong => zhuang 5%
tong => chuang 5%
tong => yun 5%
tong => yu 5%

tou => tao 42%
tou => tu 39%
tou => dao 10%
tou => yao 6%
tou => du 3%

tuk => tu 100%

tung => tong 83%
tung => dong 8%
tung => teng 4%
tung => chuang 4%

tyun => tun 57%
tyun => tuan 14%
tyun => duan 14%
tyun => zhun 14%

tyut => tuo 100%

uk => wu 100%

ung => weng 100%

waa => hua 63%
waa => wa 26%
waa => kua 7%
waa => huai 4%

waai => huai 80%
waai => wai 20%

waak => hua 57%
waak => huo 43%

waan => huan 50%
waan => wan 33%
waan => yuan 6%
waan => qiong 6%
waan => hai 6%

waang => heng 100%

waat => hua 50%
waat => gu 25%
waat => wa 25%

wai => wei 71%
wai => hui 18%
wai => yi 3%
wai => wo 3%
wai => yu 3%
wai => sui 1%
wai => xu 1%

wan => yun 56%
wan => hun 19%
wan => wen 17%
wan => yin 3%
wan => jun 3%
wan => yu 3%

wang => hong 100%

wat => yu 42%
wat => hu 17%
wat => gu 8%
wat => qu 8%
wat => wei 8%
wat => yun 8%
wat => he 8%

we => wa 100%

wik => yu 100%

wing => yong 50%
wing => rong 25%
wing => ying 12%
wing => reng 12%

wo => wo 33%
wo => he 21%
wo => wai 12%
wo => wei 8%
wo => hu 8%
wo => gua 4%
wo => huo 4%
wo => guo 4%
wo => zhua 4%

wok => huo 100%

wong => huang 66%
wong => wang 31%
wong => heng 3%

wou => hun 33%
wou => guo 33%
wou => luo 33%

wu => hu 56%
wu => wu 24%
wu => e 5%
wu => he 5%
wu => ya 2%
wu => yan 2%
wu => yu 2%
wu => xu 2%

wui => hui 75%
wui => kuai 12%
wui => wei 6%
wui => huai 6%

wun => huan 39%
wun => wan 35%
wun => yuan 17%
wun => guan 9%

wut => huo 100%

yam => ren 100%

yan => ren 100%

zaa => zha 76%
zaa => cha 7%
zaa => zhua 7%
zaa => zan 3%
zaa => wo 3%
zaa => za 3%

zaai => zhai 100%

zaak => zhai 45%
zaak => ze 27%
zaak => zha 9%
zaak => zhi 9%
zaak => zhe 9%

zaam => zhan 80%
zaam => zan 20%

zaan => zhan 33%
zaan => zhuan 33%
zaan => zan 25%
zaan => cuan 8%

zaang => zheng 100%

zaap => xi 22%
zaap => za 22%
zaap => zha 22%
zaap => shen 11%
zaap => ji 11%
zaap => shi 11%

zaat => zha 62%
zaat => ya 12%
zaat => za 12%
zaat => ga 12%

zaau => zhao 50%
zaau => zhou 19%
zaau => diao 6%
zaau => lan 6%
zaau => chao 6%
zaau => zhua 6%
zaau => zou 6%

zai => ji 50%
zai => zhi 19%
zai => zai 12%
zai => zi 6%
zai => chi 6%
zai => che 6%

zak => ze 50%
zak => zei 17%
zak => zhai 17%
zak => ce 17%

zam => zhen 83%
zam => jin 8%
zam => zen 8%

zan => zhen 89%
zan => zun 11%

zang => zheng 38%
zang => zeng 31%
zang => cheng 12%
zang => ceng 12%
zang => seng 6%

zap => zhi 50%
zap => shi 25%
zap => xie 25%

zat => zhi 67%
zat => ji 22%
zat => zhe 11%

zau => zhou 60%
zau => jiu 15%
zau => zou 15%
zau => xiu 10%

ze => zhe 40%
ze => jie 20%
ze => zhei 10%
ze => ju 10%
ze => ji 10%
ze => xie 5%
ze => ze 5%

zek => ji 57%
zek => zhi 29%
zek => xi 14%

zeng => jing 60%
zeng => zheng 30%
zeng => dan 10%

zeoi => ju 39%
zeoi => zhui 22%
zeoi => zui 14%
zeoi => xu 8%
zeoi => chuo 6%
zeoi => di 3%
zeoi => qie 3%
zeoi => chui 3%
zeoi => yu 3%

zeon => jin 35%
zeon => jun 24%
zeon => zhun 18%
zeon => zun 12%
zeon => zhen 6%
zeon => tun 6%

zeot => cu 67%
zeot => zu 33%

zeu => jiao 67%
zeu => jue 33%

zi => zhi 48%
zi => zi 24%
zi => si 9%
zi => ci 4%
zi => shi 4%
zi => chi 3%
zi => yi 1%
zi => zheng 1%
zi => duo 1%
zi => die 1%
zi => zai 1%
zi => ji 1%
zi => qi 1%

zik => ji 50%
zik => zhi 25%
zik => xi 16%
zik => zi 3%
zik => jie 3%
zik => shi 3%

zim => zhan 64%
zim => jian 36%

zin => jian 57%
zin => zhan 26%
zin => dian 9%
zin => chan 9%

zing => jing 39%
zing => zheng 39%
zing => zhen 11%
zing => sheng 3%
zing => dan 3%
zing => liang 3%
zing => zhi 3%

zip => zhe 57%
zip => zha 14%
zip => jie 14%
zip => she 14%

zit => jie 44%
zit => zhe 44%
zit => zhi 6%
zit => she 6%

ziu => jiao 43%
ziu => zhao 29%
ziu => chao 10%
ziu => qiao 5%
ziu => jue 5%
ziu => jiu 5%
ziu => shao 5%

zo => zuo 50%
zo => zu 36%
zo => zhu 14%

zoe => ze 100%

zoek => zhuo 21%
zoek => zhao 18%
zoek => qiao 12%
zoek => jiao 9%
zoek => zhe 9%
zoek => que 9%
zoek => zhu 6%
zoek => jue 6%
zoek => shao 6%
zoek => chao 3%

zoeng => zhang 62%
zoeng => jiang 26%
zoeng => xiang 8%
zoeng => chang 5%

zoi => zai 92%
zoi => ji 8%

zok => zuo 71%
zok => zao 14%
zok => zhuo 14%

zong => zhuang 59%
zong => zang 29%
zong => cang 6%
zong => chuang 6%

zou => zao 69%
zou => zu 23%
zou => zuo 8%

zui => zhui 100%

zuk => zhu 38%
zuk => zhuo 15%
zuk => zhou 12%
zuk => shu 8%
zuk => zu 8%
zuk => chuo 4%
zuk => su 4%
zuk => jiu 4%
zuk => chu 4%
zuk => xu 4%

zung => zhong 48%
zung => zong 36%
zung => chong 7%
zung => song 7%
zung => cong 2%

zyu => zhu 76%
zyu => zhao 7%
zyu => dian 3%
zyu => shu 3%
zyu => zhou 3%
zyu => zhe 3%
zyu => zhuo 3%

zyun => zhuan 54%
zyun => zun 15%
zyun => zuan 15%
zyun => chuan 8%
zyun => juan 8%

zyut => chuo 33%
zyut => duo 22%
zyut => jue 11%
zyut => chu 11%
zyut => zhui 11%
zyut => zhuo 11%

