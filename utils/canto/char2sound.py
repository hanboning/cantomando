import json, pickle

data = pickle.load(open('data/scraped_original.pickle'))

jsonout = {}
for char, (cantosounds, mandosounds) in data.iteritems():
  jsonout[char] = {}
  tmp = set() # new list of cantosounds (disambiguated)
  for sound in cantosounds:
    pos = sound.find('*')
    if (pos >= 0):
      tmp.add(sound[0:pos])
      tmp.add(sound[0:pos-1]+sound[pos+1:pos+2])
    else:
      tmp.add(sound)
  jsonout[char]['canto'] = sorted(list(tmp))

  tmp = set() # new list of mandosounds (disambiguated)
  for sound in mandosounds:
    pos = sound.find('*')
    if (pos >= 0):
      tmp.add(sound[0:pos])
      tmp.add(sound[0:pos-1]+sound[pos+1:pos+2])
    else:
      tmp.add(sound)
  jsonout[char]['mando'] = sorted(list(tmp))
print json.dumps(jsonout, separators=(',',':'), indent=1)
