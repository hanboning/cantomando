import sys

def get_canto_split(sound):
  initials = ('','b','p','m','f','d','t','n','l','g','k','ng','h','gw','kw','w',
      'z','c','s','j')
  initmaxlen = len(max(initials, key=len))
  sound = sound.strip()
  idx = 0
  while idx <= initmaxlen:
    if sound[:idx] in initials and sound[:idx+1] not in initials:
      break
    idx += 1
  return sound[:idx], sound[idx:]

def get_mando_split(sound):
  initials = ('','b','p','m','f','d','t','n','l','g','k','h','j','q','x','zh',
      'ch','sh','r','z','c','s')
  initmaxlen = len(max(initials, key=len))
  sound = sound.strip()
  if sound in ('ri', 'chi', 'shi', 'zhi'):
    sound += 'r'
  elif sound in ('ci', 'si', 'zi'):
    sound += 'h'
  idx = 0
  while idx <= initmaxlen:
    if sound[:idx] in initials and sound[:idx+1] not in initials:
      break
    idx += 1
  return sound[:idx], sound[idx:]

if __name__ == '__main__':
  if len(sys.argv) != 1:
    print('Usage: python initialfinalizer.py')
  for sound in ('bian', 'yuan', 'chi', 'si'):
    sound = sound.strip()
    print sound, get_mando_split(sound)
  for sound in ('aam', 'aai', 'ngaam', 'hoeng', 'syut', 'jyut'):
    sound = sound.strip()
    print sound, get_canto_split(sound)
