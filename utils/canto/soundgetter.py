import json, os, sys, urllib2
import grequests

def get_sounds(initials, finals, destdir):
  try: os.makedirs(destdir) # make directory if not exists
  except: pass

  sounds = ['{}{}{}'.format(i, f, t) for i in initials for f in finals for t in range(1,7)]
  interval = 6
  for i in range(0, len(sounds), interval):
    batch = sounds[i:i+interval]
    print 'making network requests for {}...'.format(batch)
    sound_url = 'http://humanum.arts.cuhk.edu.hk/Lexis/lexi-can/sound/{}.wav'
    urls = (grequests.get(sound_url.format(s)) for s in batch)
    for sound, response in zip(batch, grequests.map(urls)):
      if response.status_code != 200:
        print ' {} not found, skipping...'.format(sound)
        continue
      print ' saving {} to a file...'.format(sound)
      soundfp = open('{}/{}.wav'.format(destdir, sound), 'wb')
      soundfp.write(response.content)
      soundfp.close

if __name__ == '__main__':
  if (len(sys.argv) != 3):
    print('Usage: python soundgetter.py <canto initials and finals file> <dest directory>')
    sys.exit(1)
  initials_finals = json.load(open(sys.argv[1]))
  get_sounds(initials_finals['initials'], initials_finals['finals'], sys.argv[2])
