import pickle
import random
import requests
import sys
import time
from bs4 import BeautifulSoup

def scrape_test():
  res = {}
  scrape(open('gu.html', 'r').read(), res)
  return res
def scrape_all(pinyins):
  random.seed()
  res = {}
  url = 'http://www.cantonese.sheik.co.uk/scripts/wordsearch.php?level=0'
  for pinyin in pinyins:
    r = requests.post(url, data={'TEXT': pinyin, 'SEARCHTYPE': 5})
    r.encoding = 'utf-8'

    print('scraping {0}'.format(pinyin))
    scrape(r.text, res)

    seconds = random.random() * 5.0
    print('waiting {0} seconds...'.format(seconds))
    time.sleep(seconds)
  return res
def scrape(text, res):
  soup = BeautifulSoup(text)
  table = soup.find_all('table')[1]
  for tr in table.find_all('tr')[3:]:
    character = tr.find_all('td')[1].span.text
    if character in res:
      continue
    print(character.encode('utf-8'))
    charres = [[], []]
    jyutpings = tr.find_all('td')[2].span.text
    for jp in jyutpings.split(' '):
      if jp == '' or jp.find('{') >= 0: continue
      charres[0].append(jp)
    pinyins = tr.find_all('td')[3].span.text
    for py in pinyins.split(' '):
      if py == '' or py.find('{') >= 0: continue
      charres[1].append(py)
    res[character] = charres
  return res

if __name__ == '__main__':
  if (len(sys.argv) != 2):
    print('Usage: python scraper.py <mando pinyin file>')
    sys.exit(1)
  pinyins = set()
  for line in open(sys.argv[1]):
    pinyins.add(line.strip())
#  res = scrape_test()
  res = scrape_all(pinyins)
  pickle.dump(res, open('out.pickle', 'wb'))
