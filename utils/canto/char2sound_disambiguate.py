import json, sys

if len(sys.argv) != 3:
  print 'Usage: python char2sound_disambiguate.py <master json file> <out dir>'
  sys.exit(1)

data = {}
with open(sys.argv[1]) as char2sound_file:
  for char, languages in json.load(char2sound_file).iteritems():
    for language, sounds in languages.iteritems():
      if language not in data: data[language] = {}
      data[language][char] = sounds
for language, char2sounds in data.iteritems():
  with open('{}{}.json'.format(sys.argv[2], language), 'w') as outfile:
    json.dump(char2sounds, outfile)
