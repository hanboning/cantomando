import pickle
import re
import sys

from initialfinalizer import get_canto_split, get_mando_split

def analyze_split(data):
  mando = {}
  canto = {}
  mandosplits = {}
  cantosplits = {}

  for character, (cantos, mandos) in data.iteritems():

    for c in cantos:
      r = re.search('\d', c)
      c = c[:r.start()]
      if c not in cantosplits:
        cantosplits[c] = get_canto_split(c)
      ci, cf = cantosplits[c]
      if ci not in canto:
        canto[ci] = {}
      if cf not in canto:
        canto[cf] = {}
      for m in mandos:
        r = re.search('\d', m)
        m = m[:r.start()]
        if m not in mandosplits:
          mandosplits[m] = get_mando_split(m)
        mi, mf = mandosplits[m]
        if mi not in canto[ci]:
          canto[ci][mi] = []
        if mf not in canto[cf]:
          canto[cf][mf] = []
        canto[ci][mi].append(character)
        canto[cf][mf].append(character)

    for m in mandos:
      r = re.search('\d', m)
      m = m[:r.start()]
      if m not in mandosplits:
        mandosplits[m] = get_mando_split(m)
      mi, mf = mandosplits[m]
      if mi not in mando:
        mando[mi] = {}
      if mf not in mando:
        mando[mf] = {}
      for c in cantos:
        r = re.search('\d', c)
        c = c[:r.start()]
        if c not in cantosplits:
          cantosplits[c] = get_canto_split(c)
        ci, cf = cantosplits[c]
        if ci not in mando[mi]:
          mando[mi][ci] = []
        if cf not in mando[mf]:
          mando[mf][cf] = []
        mando[mi][ci].append(character)
        mando[mf][cf].append(character)

  return mando, canto
def analyze_toneless(data):
  mando = {}
  canto = {}
  for character, (cantos, mandos) in data.iteritems():
    for c in cantos:
      r = re.search('\d', c)
      c = c[:r.start()]
      if c not in canto:
        canto[c] = {}
      for m in mandos:
        r = re.search('\d', m)
        if not r: continue
        m = m[:r.start()]
        if m not in canto[c]:
          canto[c][m] = []
        canto[c][m].append(character)
    for m in mandos:
      r = re.search('\d', m)
      if not r: continue
      m = m[:r.start()]
      if m not in mando:
        mando[m] = {}
      for c in cantos:
        r = re.search('\d', c)
        c = c[:r.start()]
        if c not in mando[m]:
          mando[m][c] = []
        mando[m][c].append(character)
  return mando, canto
def analyze_toneless_disam(data):
  mando = {}
  canto = {}
  for character, (cantos, mandos) in data.iteritems():
    idx = min(len(cantos), len(mandos))
    for i in xrange(idx):
      c = cantos[i]
      r = re.search('\d', c)
      if not r: continue
      c = c[:r.start()]
      m = mandos[i]
      r = re.search('\d', m)
      if not r: continue
      m = m[:r.start()]

      if c not in canto: canto[c] = {}
      if m not in mando: mando[m] = {}
      if c not in mando[m]: mando[m][c] = []
      if m not in canto[c]: canto[c][m] = []

      canto[c][m].append(character)
      mando[m][c].append(character)
  return mando, canto
def analyze(data):
  mando = {}
  canto = {}
  for character, (cantos, mandos) in data.iteritems():
    for c in cantos:
      if c not in canto:
        canto[c] = {}
      for m in mandos:
        if m not in canto[c]:
          canto[c][m] = []
        canto[c][m].append(character)
    for m in mandos:
      if m not in mando:
        mando[m] = {}
      for c in cantos:
        if c not in mando[m]:
          mando[m][c] = []
        mando[m][c].append(character)
  return mando, canto

if __name__ == '__main__':
  if (len(sys.argv) != 2):
    print('Usage: python analyzer.py <pickled data file>')
    sys.exit(1)
  data = pickle.load(open(sys.argv[1]))

  mando, canto = analyze(data)
  pickle.dump(mando, open('data/mando2canto.pickle', 'wb'))
  pickle.dump(canto, open('data/canto2mando.pickle', 'wb'))

  mando, canto = analyze_toneless(data)
  pickle.dump(mando, open('data/mando2canto_toneless.pickle', 'wb'))
  pickle.dump(canto, open('data/canto2mando_toneless.pickle', 'wb'))

  mando, canto = analyze_toneless_disam(data)
  pickle.dump(mando, open('data/mando2canto_toneless_disam.pickle', 'wb'))
  pickle.dump(canto, open('data/canto2mando_toneless_disam.pickle', 'wb'))

  mando, canto = analyze_split(data)
  pickle.dump(mando, open('data/mando2canto_split.pickle', 'wb'))
  pickle.dump(canto, open('data/canto2mando_split.pickle', 'wb'))
