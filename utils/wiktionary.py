import codecs, sys
import xml.etree.ElementTree as ET

def print_page(path):
  print 'searching for Chinese characters...'
  curpage_lines = []
  for line in open(path):
    if line.find('<page>') >= 0: # start new page
      curpage_lines = []
    curpage_lines.append(line)

    if line.find('</page>') >= 0: # have full page
      curpage = ''.join(curpage_lines)
      root = ET.fromstring(curpage)
      title = root.find('title').text
      print title, type(title)
      if len(title) == 1 and (u'\u4e00' <= title[0] <= u'\u9fff' or u'\u3400' <= title[0] <= u'\u4dff'):
        print 'CJK!'

if __name__ == '__main__':
  path = sys.argv[1]
  print_page(path)
